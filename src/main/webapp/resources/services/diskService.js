angular.module('diskService', [])
    .factory('DiskFactory', function($http) {
        var diskFactory = {};

        diskFactory.findAll = function(limit, offset, free) {
            var config = {
                params: {
                    limit: limit,
                    offset: offset,
                    free: free
                }
            };

            return $http.get('api/disk', config)
                .then(function(res) {
                    return res.data;
                })
                .catch(function(err) {
                    return err;
                });
        };

        diskFactory.create = function(disk) {
          return $http.post('api/disk/', disk)
              .then(function (res) {
                  return res.data;
              });
        };

        diskFactory.update = function(disk, id) {
            return $http.post('api/disk/' + id, disk)
                .then(function (res) {
                    return res.data;
                });
        };

        diskFactory.getById = function(id) {
            return $http.get('api/disk/' + id)
                .then(function (res) {
                    return res.data;
                });
        };


        diskFactory.takeDisk = function(disk) {
          return $http.post('api/disk/' + disk.id +'/take')
              .then(function (res) {
                  return res.data;
              });
        };

        diskFactory.freeDisk = function(disk) {
            return $http.post('api/disk/' + disk.id +'/free')
                .then(function (res) {
                    return res.data;
                });
        };

        return diskFactory;
    });
