angular.module('authService', [])
    .factory('Auth', function($http, $cacheFactory, $q, AuthToken, $window) {
        var authFactory = {};

        authFactory.login = function(username, password) {
            var config = {
                ignoreAuthModule: 'ignoreAuthModule',
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            };

            return $http.post('api/auth/login', $.param({
                username: username,
                password: password
            }), config)
                .then(function(res) {
                    console.log(res);
                    AuthToken.setToken(res.data.username);
                    return res.data;
                });
        };

        authFactory.signup = function(username, password, fullname) {
            return $http.post('api/person', {
                username: username,
                password: password,
                fullname: fullname
            })
            .then(function(data) {
                return data.data;
            });
        };


        authFactory.resetCache = function() {
            $cacheFactory.get('$http').removeAll();
        };

        authFactory.logout = function() {
            return $http.post('api/auth/logout')
                .then(function(res) {
                    AuthToken.setToken();
                    $cacheFactory.get('$http').removeAll();
                    return res.data;
                })
                .catch(function(err) {
                    return err;
                });
        };

        authFactory.isLoggedIn = function() {
            return !!AuthToken.getToken();

        };


        authFactory.getUser = function() {
            if (AuthToken.getToken())
                return $http.get('api/auth/me', { cache: true })
                    .then(function(res) {
                        return res.data;
                    })
                    .catch(function() {
                        AuthToken.setToken()
                    });
        };

        return authFactory;
    })
    .factory('AuthToken', function($cookies) {
        var authTokenFactory = {};

        authTokenFactory.getToken = function() {
            return $cookies.get('token');
        };

        authTokenFactory.setToken = function(token) {
            if (token)
                $cookies.put('token', token);
            else
                $cookies.remove('token');
        };

        return authTokenFactory;
    });
