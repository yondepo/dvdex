angular.module('personService', [])
    .factory('PersonFactory', function($http) {
        var personFactory = {};

        personFactory.getOwnedDisks = function(username) {
            return $http.get('api/person/' + username + '/disks/owned')
                .then(function (res) {
                    return res.data;
                });
        };

        personFactory.getTakenDisks = function(username) {
            return $http.get('api/person/' + username + '/disks/taken')
                .then(function (res) {
                    return res.data;
                });
        };

        return personFactory;
    });
