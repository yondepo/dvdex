'use strict';

angular.module('profileModule', [])
    .controller('ProfileController', function ($scope, $location, $routeParams, PersonFactory) {
        var vm = this;
        vm.username = $routeParams.username;
        vm.diskList = [];
        vm.disksType = "owned";

        var getDiscs = function () {
            if (vm.disksType === "taken") {
                return PersonFactory.getTakenDisks(vm.username);
            }
            return PersonFactory.getOwnedDisks(vm.username);
        };

        getDiscs()
            .then(function (data) {
                vm.diskList = data;
            });

        vm.diskTypeChanged = function() {
            getDiscs()
                .then(function (data) {
                    vm.diskList = data;
                });
        };
    });
