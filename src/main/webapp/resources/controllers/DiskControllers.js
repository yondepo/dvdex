'use strict';

angular.module('diskModule', [])
    .controller('DiskController', function($scope, $location, $routeParams, DiskFactory) {
        var vm = this;
        vm.disk = {};

        if ($routeParams.id) {
            vm.caption = 'Обновить';
            vm.error = '';
            DiskFactory.getById($routeParams.id)
                .then(function (data) {
                    vm.disk = data;
                    vm.doSubmit = function () {
                        DiskFactory.update(vm.disk, $routeParams.id)
                            .then(function () {
                                $location.path('/');
                                alert('Диск обновлён')
                            })
                            .catch(function (err) {
                                vm.error = err.data.error;
                            });
                    }
                });
        } else {
            vm.caption = 'Создать';
            vm.error = '';
            vm.doSubmit = function () {
                DiskFactory.create(vm.disk)
                    .then(function () {
                        $location.path('/');
                        alert('Диск создан');
                    })
                    .catch(function (err) {
                        vm.error = err.data.error;
                    });
            }
        }
    })
    .controller('DiskListController', function ($scope, $location, DiskFactory) {
        var vm = this;
        vm.count = 0;
        vm.diskList = [];
        vm.freeFilter = true;

        DiskFactory.findAll(20, 0, vm.freeFilter)
            .then(function(data) {
                vm.diskList.push.apply(vm.diskList, data);
                vm.count = 20;
            });

        vm.filterChanged = function() {
            DiskFactory.findAll(20, 0, vm.freeFilter)
                .then(function(data) {
                    vm.diskList = data;
                    vm.count = 20;
                });
        };

        vm.loadMore = function() {
            DiskFactory.findAll(20, vm.count, vm.freeFilter)
                .then(function(data) {
                    vm.diskList.push.apply(vm.diskList, data);
                    vm.count += 20;
                });
        };
    });

