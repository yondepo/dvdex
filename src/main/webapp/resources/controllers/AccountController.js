'use strict';

angular.module('accountModule', [])
    .controller('AccountController', function ($scope, $location, Auth) {
        var vm = this;
        vm.loginData = {};

        var checkLogin = function() {
            vm.loggedIn = Auth.isLoggedIn();
            if (!vm.loggedIn) return;
            Auth.getUser()
                .then(function(data) {
                    vm.user = data;
                })
                .catch(function(err) {
                    vm.error = err.data.error;
                });
        };

        checkLogin();
        $scope.$on('$routeChangeStart', checkLogin);

        vm.doLogin = function() {
            vm.error = '';

            Auth.login(vm.loginData.username, vm.loginData.password)
                .then(function() {
                    $location.path('/');
                })
                .catch(function(err) {
                    vm.error = err.data.error;
                });
        };

        vm.doLogout = function() {
            Auth.logout()
                .then(function() {
                    vm.user = {};
                    $location.path('/');
                });
        };

        vm.doSignup = function() {
            vm.error = '';

            Auth.signup(vm.loginData.username, vm.loginData.password)
                .then(function() {
                    vm.doLogin();
                })
                .catch(function(err) {
                    vm.error = err.data.error;
                });
        }
    });
