'use strict';

var app = angular.module('dvdApp', [
    'ngRoute',
    'ngCookies',
    'authService',
    'diskService',
    'diskModule',
    'diskDirModule',
    'profileModule',
    'personService',
    'accountModule'
])
    .config(function($httpProvider, $routeProvider, $locationProvider) {
        $routeProvider
            .when('/login', {
                templateUrl: 'resources/views/loginPage.html',
                controller: 'AccountController',
                controllerAs: 'loginCtrl'
            })
            .when('/signup', {
                templateUrl: 'resources/views/signupPage.html',
                controller: 'AccountController',
                controllerAs: 'loginCtrl'
            })
            .when('/', {
                templateUrl: 'resources/views/diskListPage.html',
                controller: 'DiskListController',
                controllerAs: 'diskListCtrl'
            })
            .when('/disk/new', {
                templateUrl: 'resources/views/diskPage.html',
                controller: 'DiskController',
                controllerAs: 'diskCtrl'
            })
            .when('/disk/:id', {
                templateUrl: 'resources/views/diskPage.html',
                controller: 'DiskController',
                controllerAs: 'diskCtrl'
            })
            .when('/profile/:username', {
                templateUrl: 'resources/views/profilePage.html',
                controller: 'ProfileController',
                controllerAs: 'profileCtrl'
            });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: true
        });
    });