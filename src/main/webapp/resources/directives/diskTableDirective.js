'use strict';

angular.module('diskDirModule', [])
    .directive('row', function(DiskFactory) {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            t: '=src',
            current: '=user'
        },
        templateUrl: 'resources/directives/disksTable.html',
        link: function(scope, element, attrs) {
            scope.doTakeDisk = function(disk) {
                DiskFactory.takeDisk(disk)
                    .then(function (data) {
                        disk.captor = data.captor;
                    })
                    .catch(function(err) {
                        alert(err.data.error);
                    });
            };

            scope.doFreeDisk = function(disk) {
                DiskFactory.freeDisk(disk)
                    .then(function (data) {
                        disk.captor = data.captor;
                    })
                    .catch(function(err) {
                        alert(err.data.error);
                    });
            };
        }
    };
});