package me.yondepo.web;

import me.yondepo.domain.Disk;
import me.yondepo.domain.Person;
import me.yondepo.exceptions.EntityValidationException;
import me.yondepo.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/person")
public class PersonController {

    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Person> list() {
        return personService.list();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Person postPerson(@Valid @RequestBody Person person, Errors errors) {
        if (errors.hasErrors()) {
            throw new EntityValidationException(errors);
        }

        return personService.create(person);
    }

    @RequestMapping(value = "/{username}")
    public Person getPerson(@PathVariable String username) {
        return personService.getByUsername(username);
    }

    @RequestMapping(value = "/{username}/disks/owned")
    public List<Disk> getOwnedDiscs(@PathVariable String username) {
        return personService.getOwnedDisks(username);
    }

    @RequestMapping(value = "/{username}/disks/taken")
    public List<Disk> getTakenDiscs(@PathVariable String username) {
        return personService.getTakenDisks(username);
    }
}
