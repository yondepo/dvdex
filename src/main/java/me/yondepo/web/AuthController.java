package me.yondepo.web;

import me.yondepo.domain.Person;
import me.yondepo.services.PersonSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private final PersonSessionService personSessionService;

    @Autowired
    public AuthController(PersonSessionService personSessionService) {
        this.personSessionService = personSessionService;
    }

    @RequestMapping(value = "/me")
    public Person getMe() {
        return personSessionService.getLoggedInPerson();
    }
}
