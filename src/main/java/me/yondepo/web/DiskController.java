package me.yondepo.web;

import me.yondepo.domain.Disk;
import me.yondepo.exceptions.EntityValidationException;
import me.yondepo.services.DiskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/disk")
public class DiskController {

    private final DiskService diskService;

    @Autowired
    public DiskController(DiskService diskService) {
        this.diskService = diskService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Disk> list(
            @RequestParam(defaultValue = "0") int offset,
            @RequestParam(defaultValue = "20") int limit,
            @RequestParam(defaultValue = "false") boolean free) {
        return diskService.list(offset, limit, free);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Disk getById(@PathVariable int id) {
        return diskService.getById(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Disk create(@Valid @RequestBody Disk disk, Errors errors) {
        if (errors.hasErrors()) {
            throw new EntityValidationException(errors);
        }

        return diskService.create(disk);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public Disk update(
            @PathVariable int id,
            @Valid @RequestBody Disk disk, Errors errors) {
        if (errors.hasErrors()) {
            throw new EntityValidationException(errors);
        }

        disk.setId(id);
        return diskService.update(disk);
    }

    @RequestMapping(value = "/{id}/take",method = RequestMethod.POST)
    public Disk take(@PathVariable int id) {
        return diskService.take(id);
    }

    @RequestMapping(value = "/{id}/free",method = RequestMethod.POST)
    public Disk free(@PathVariable int id) {
        return diskService.free(id);
    }

}
