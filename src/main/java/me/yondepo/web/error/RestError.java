package me.yondepo.web.error;

public class RestError {
    private final String error;

    public RestError(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }
}