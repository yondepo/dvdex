package me.yondepo.web;

import me.yondepo.exceptions.*;
import me.yondepo.web.error.RestError;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.List;
import java.util.StringJoiner;

@ControllerAdvice
public class ExceptionHandlerController {


    @ExceptionHandler(PersonNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public @ResponseBody RestError handlePersonNotFound(PersonNotFoundException e) {
        String username = e.getUsername();
        return new RestError("User " + username + " not found");
    }

    @ExceptionHandler(PersonDuplicateException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody RestError handlePersonDuplicate(PersonDuplicateException e) {
        String username = e.getUsername();
        return new RestError("Username " + username + " already exists");
    }

    @ExceptionHandler(EntityValidationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody
    RestError handleEntityValidationFailure(EntityValidationException e) {
        List<FieldError> errors = e.getErrors().getFieldErrors();
        StringJoiner sj = new StringJoiner("\n");
        for (FieldError error : errors) {
            sj.add(error.getDefaultMessage());
        }
        return new RestError(sj.toString());
    }

    @ExceptionHandler(DiskNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public @ResponseBody RestError handleDiskNotFound(DiskNotFoundException e) {
        int id = e.getId();
        return new RestError("Disk №" + id + " not found");
    }

    @ExceptionHandler(DiskManageFailureException.class)
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public @ResponseBody RestError handleDiskManageFailure(DiskManageFailureException e) {
        int id = e.getId();
        return new RestError("You cannot manage disk №" + id );
    }


    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody RestError handleOtherExceptions(Exception e) {
        return new RestError("Internal server error: " + e.getMessage());
    }

}
