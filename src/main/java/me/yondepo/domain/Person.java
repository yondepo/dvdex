package me.yondepo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "users")
public class Person {

    @Id
    @Column(name = "username", unique = true)
    @NotNull
    @Size(min = 3, max = 12, message = "{person.username.size}")
    private String username;

    @NotNull
    @Size(min = 3, max = 12, message = "{person.password.size}")
    @Column(name = "password")
    private String password;

    @JsonIgnore
    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
    @OrderBy(value = "id desc")
    private List<Disk> ownedDisks;

    @JsonIgnore
    @OneToMany(mappedBy = "captor", fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST })
    @OrderBy(value = "id desc")
    private List<Disk> takenDisks;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Disk> getOwnedDisks() {
        return ownedDisks;
    }

    public void setOwnedDisks(List<Disk> ownedDisks) {
        this.ownedDisks = ownedDisks;
    }

    public List<Disk> getTakenDisks() {
        return takenDisks;
    }

    public void setTakenDisks(List<Disk> takenDisks) {
        this.takenDisks = takenDisks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        return username != null ? username.equals(person.username) : person.username == null;
    }

    @Override
    public int hashCode() {
        return username != null ? username.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Person{" +
                "username='" + username + '\'' +
                '}';
    }
}
