package me.yondepo.domain;

import org.hibernate.annotations.Filter;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.Range;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "disks")
public class Disk {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    private int id;

    @Column
    @NotNull
    @NotEmpty
    @Size(min = 1, max = 30, message = "{disk.name.size}")
    private String name;

    @Size(max = 160, message = "{disk.description.size}")
    @Column
    private String description;

    @Range(min = 1900, max = 2030, message = "{disk.year.range}")
    @Column
    private Integer year;

    @ManyToOne
    @JoinColumn(name = "owner")
    private Person owner;

    @ManyToOne
    @JoinColumn(name = "captor")
    private Person captor;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public Person getCaptor() {
        return captor;
    }

    public void setCaptor(Person captor) {
        this.captor = captor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Disk disk = (Disk) o;

        return id == disk.id;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return "Disk{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", year='" + year + '\'' +
                '}';
    }
}
