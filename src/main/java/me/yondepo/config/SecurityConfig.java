package me.yondepo.config;

import me.yondepo.config.security.AuthFailureHandler;
import me.yondepo.config.security.AuthSuccessHandler;
import me.yondepo.config.security.RestAuthEntryPoint;
import me.yondepo.services.PersonDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final RestAuthEntryPoint restAuthEntryPoint;
    private final AuthSuccessHandler authSuccessHandler;
    private final AuthFailureHandler authFailureHandler;
    private final PersonDetailsService personDetailsService;

    @Autowired
    public SecurityConfig(RestAuthEntryPoint restAuthEntryPoint, AuthSuccessHandler authSuccessHandler, AuthFailureHandler authFailureHandler, PersonDetailsService personDetailsService) {
        this.restAuthEntryPoint = restAuthEntryPoint;
        this.authSuccessHandler = authSuccessHandler;
        this.authFailureHandler = authFailureHandler;
        this.personDetailsService = personDetailsService;
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(personDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .csrf().disable()
            .headers().disable()
            .formLogin()
                .loginProcessingUrl("/api/auth/login")
                .usernameParameter("username")
                .passwordParameter("password")
                .successHandler(authSuccessHandler)
                .failureHandler(authFailureHandler)
                .permitAll()
                .and()
            .exceptionHandling()
                .authenticationEntryPoint(restAuthEntryPoint)
                .and()
            .logout()
                .logoutUrl("/api/auth/logout")
                .logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler())
                .permitAll()
                .and()
            .rememberMe()
                .tokenValiditySeconds(2419200)
                .key("key")
                .and()
            .authorizeRequests()
            .antMatchers(HttpMethod.POST,"/api/disk/**").authenticated()
            .antMatchers(HttpMethod.DELETE,"/api/disk/**").authenticated()
            .antMatchers(HttpMethod.GET, "/api/auth/me").authenticated()
            .anyRequest().permitAll();
    }
}
