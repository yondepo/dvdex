package me.yondepo.config.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.yondepo.domain.Person;
import me.yondepo.services.PersonSessionService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@Component
public class AuthSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    private static final Logger logger = Logger.getLogger(AuthSuccessHandler.class);

    private final PersonSessionService personSessionService;

    @Autowired
    public AuthSuccessHandler(PersonSessionService personSessionService) {
        this.personSessionService = personSessionService;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);

        Person person = personSessionService.getLoggedInPerson();

        logger.info(person.getUsername() + " connected");

        PrintWriter writer = response.getWriter();
        ObjectMapper mapper = new MappingJackson2HttpMessageConverter().getObjectMapper();

        mapper.writeValue(writer, person);
        writer.flush();
        writer.close();
    }
}
