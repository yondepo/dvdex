package me.yondepo.data.impl;

import me.yondepo.data.PersonRepository;
import me.yondepo.domain.Person;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PersonRepositoryImpl implements PersonRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public PersonRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public List<Person> list() {
        return getSession().createQuery("select p from Person p", Person.class)
                .getResultList();
    }

    @Override
    public Person create(Person person) {
        getSession().persist(person);
        return person;
    }

    @Override
    public Person getByUsername(String username) {
        return getSession().find(Person.class, username);
    }
}
