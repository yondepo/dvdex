package me.yondepo.data.impl;

import me.yondepo.data.DiskRepository;
import me.yondepo.domain.Disk;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class DiskRepositoryImpl implements DiskRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public DiskRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    @Override
    public List<Disk> list(int offset, int limit, boolean free) {
        Query<Disk> query;
        if (free) {
            query = getSession().createQuery("select d from Disk d where d.captor = null order by d.id desc", Disk.class);
        } else {
            query = getSession().createQuery("select d from Disk d order by d.id desc ", Disk.class);
        }
        return query
            .setFirstResult(offset)
            .setMaxResults(limit)
            .getResultList();
    }

    @Override
    public Disk getById(int id) {
        return getSession().find(Disk.class, id);
    }

    @Override
    public Disk create(Disk disk) {
        getSession().persist(disk);
        return disk;
    }

    @Override
    public Disk update(Disk disk) {
        getSession().update(disk);
        return disk;
    }
}
