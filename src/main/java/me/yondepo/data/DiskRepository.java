package me.yondepo.data;

import me.yondepo.domain.Disk;

import java.util.List;

public interface DiskRepository {

    List<Disk> list(int offset, int limit, boolean free);

    Disk getById(int id);

    Disk create(Disk disk);

    Disk update(Disk disk);

}
