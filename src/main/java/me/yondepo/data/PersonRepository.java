package me.yondepo.data;

import me.yondepo.domain.Person;

import java.util.List;

public interface PersonRepository {

    List<Person> list();

    Person create(Person person);

    Person getByUsername(String username);

}
