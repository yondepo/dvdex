package me.yondepo.exceptions;

public class DiskManageFailureException extends AbstractDiskException {
    public DiskManageFailureException(int id) {
        super(id);
    }
}

