package me.yondepo.exceptions;

public abstract class AbstractPersonException extends RuntimeException {

    private final String username;

    AbstractPersonException(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
}
