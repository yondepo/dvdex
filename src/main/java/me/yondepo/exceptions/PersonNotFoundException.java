package me.yondepo.exceptions;

public class PersonNotFoundException extends AbstractPersonException {
    public PersonNotFoundException(String username) {
        super(username);
    }
}

