package me.yondepo.exceptions;

public class PersonDuplicateException extends AbstractPersonException {
    public PersonDuplicateException(String username) {
        super(username);
    }
}

