package me.yondepo.exceptions;

public abstract class AbstractDiskException extends RuntimeException {
    private final int id;

    AbstractDiskException(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
