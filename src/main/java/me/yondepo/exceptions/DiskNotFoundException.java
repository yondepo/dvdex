package me.yondepo.exceptions;

public class DiskNotFoundException extends AbstractDiskException {

    public DiskNotFoundException(int id) {
        super(id);
    }
}