package me.yondepo.exceptions;

import org.springframework.validation.Errors;

public class EntityValidationException extends RuntimeException {

    private final Errors errors;

    public EntityValidationException(Errors errors) {
        this.errors = errors;
    }

    public Errors getErrors() {
        return errors;
    }
}
