package me.yondepo.services;

import me.yondepo.domain.Person;

public interface PersonSessionService {

    String getLoggedInUsername();

    Person getLoggedInPerson();

}
