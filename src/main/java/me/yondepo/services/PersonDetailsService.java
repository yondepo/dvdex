package me.yondepo.services;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface PersonDetailsService extends UserDetailsService {
}
