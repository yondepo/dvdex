package me.yondepo.services.impl;

import me.yondepo.data.DiskRepository;
import me.yondepo.domain.Disk;
import me.yondepo.domain.Person;
import me.yondepo.exceptions.DiskManageFailureException;
import me.yondepo.exceptions.DiskNotFoundException;
import me.yondepo.services.DiskService;
import me.yondepo.services.PersonSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DiskServiceImpl implements DiskService {

    private final DiskRepository diskRepository;
    private final PersonSessionService personSessionService;

    @Autowired
    public DiskServiceImpl(DiskRepository diskRepository, PersonSessionService personSessionService) {
        this.diskRepository = diskRepository;
        this.personSessionService = personSessionService;
    }

    @Override
    public List<Disk> list(int offset, int limit, boolean free) {
        return diskRepository.list(offset, limit, free);
    }

    @Override
    public Disk getById(int id) {
        Disk disk = diskRepository.getById(id);
        if (disk == null) {
            throw new DiskNotFoundException(id);
        }
        return disk;
    }

    @Override
    public Disk create(Disk disk) {
        Person owner = personSessionService.getLoggedInPerson();
        disk.setOwner(owner);
        return diskRepository.create(disk);
    }

    @Override
    public Disk update(Disk disk) {
        Disk attached = this.getById(disk.getId());
        Person current = personSessionService.getLoggedInPerson();

        if (attached.getOwner() != current) {
            throw new DiskManageFailureException(disk.getId());
        }

        attached.setName(disk.getName());
        attached.setDescription(disk.getDescription());
        attached.setYear(disk.getYear());
        return diskRepository.update(attached);
    }

    @Override
    public Disk take(int id) {
        Disk disk = this.getById(id);
        if (disk.getCaptor() != null) {
            throw new DiskManageFailureException(id);
        }

        Person captor = personSessionService.getLoggedInPerson();
        disk.setCaptor(captor);
        return diskRepository.update(disk);
    }

    @Override
    public Disk free(int id) {
        Person current = personSessionService.getLoggedInPerson();
        Disk disk = this.getById(id);
        if (disk.getCaptor() != current) {
            throw new DiskManageFailureException(id);
        }

        disk.setCaptor(null);
        return diskRepository.update(disk);
    }
}
