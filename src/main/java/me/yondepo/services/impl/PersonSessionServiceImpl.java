package me.yondepo.services.impl;

import me.yondepo.domain.Person;
import me.yondepo.services.PersonService;
import me.yondepo.services.PersonSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class PersonSessionServiceImpl implements PersonSessionService {

    private final PersonService personService;

    @Autowired
    public PersonSessionServiceImpl(PersonService personService) {
        this.personService = personService;
    }

    @Override
    public String getLoggedInUsername() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.getName();
    }

    @Override
    public Person getLoggedInPerson() {
        String username = getLoggedInUsername();
        return personService.getByUsername(username);
    }
}
