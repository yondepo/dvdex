package me.yondepo.services.impl;

import me.yondepo.data.PersonRepository;
import me.yondepo.domain.Disk;
import me.yondepo.domain.Person;
import me.yondepo.exceptions.PersonDuplicateException;
import me.yondepo.exceptions.PersonNotFoundException;
import me.yondepo.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;

    @Autowired
    public PersonServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public List<Person> list() {
        return personRepository.list();
    }

    @Override
    public Person create(Person person) {
        if (personRepository.getByUsername(person.getUsername()) != null) {
            throw new PersonDuplicateException(person.getUsername());
        }
        return personRepository.create(person);
    }

    @Override
    public Person getByUsername(String username) {
        Person person = personRepository.getByUsername(username);
        if (person == null) {
            throw new PersonNotFoundException(username);
        }
        return person;
    }

    @Override
    public List<Disk> getOwnedDisks(String username) {
        Person person = getByUsername(username);
        person.getOwnedDisks().size();
        return person.getOwnedDisks();
    }

    @Override
    public List<Disk> getTakenDisks(String username) {
        Person person = getByUsername(username);
        person.getTakenDisks().size();
        return person.getTakenDisks();
    }
}
