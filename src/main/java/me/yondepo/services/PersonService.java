package me.yondepo.services;

import me.yondepo.domain.Disk;
import me.yondepo.domain.Person;
import java.util.List;

public interface PersonService {

    List<Person> list();

    Person create(Person person);

    Person getByUsername(String username);

    List<Disk> getOwnedDisks(String username);

    List<Disk> getTakenDisks(String username);

}
