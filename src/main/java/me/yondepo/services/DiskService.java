package me.yondepo.services;

import me.yondepo.domain.Disk;

import java.util.List;

public interface DiskService {

    List<Disk> list(int offset, int limit, boolean free);

    Disk getById(int id);

    Disk create(Disk disk);

    Disk update(Disk disk);

    Disk take(int id);

    Disk free(int id);

}
