### DVD exchange

Ссылка на проект на bitbucket: https://bitbucket.org/yondepo/dvdex

Инструкция:

1 Скачать проект [вручную](https://bitbucket.org/yondepo/dvdex/downloads/) либо с помощью команды:

```
git clone https://yondepo@bitbucket.org/yondepo/dvdex.git
```

2 Запусить приложения с помощью Maven и встроенного tomcat:

```
cd dvdex
mvn tomcat7:run
```

3 Перейти на localhost:8080/dvdex/.


Для запуска на другом порте использовать флаг -Dmaven.tomcat.port=8081.
Для генерации war-файла использовать mvn package

### Описание функционала

* Использованные технологии: Spring Core, MVC, Security; Hibernate; AngularJS, Bootstrap.
* Встроенная БД HSQLDB. Начальный список пользователей и дисков загружается.
* Авторизация и регистрация пользователей.
* Авторизованные пользователи могут создавать/редактировать и брать/отдавать диски.
* Список всех дисков на странице "Список дисков", есть возможность показать как свободные, так и все диски, а также подгружать список частями. 
* Список созданных и взятых дисков на странице пользователя.
* Каждая строка списка включает краткую информацию о диске со ссылками на страницу диска и создателя, а также колонку со статусом диска, которая показывает, что диск либо взят другим пользователем, либо свободен, позволяя взять, либо взят текущим пользователем, позволяя отдать.